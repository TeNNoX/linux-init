FROM debian

RUN apt update \
    && apt install -y --no-install-recommends ansible gpg gpg-agent \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app
COPY *.yml ./

CMD ansible-playbook -i localhost, local.yml