#!/bin/bash
set -euxo pipefail

docker build -t debian-ansible -f debian.dockerfile .
#docker run --rm -it -v $(pwd):/app debian-ansible ansible-playbook -i localhost, local.yml
docker run --rm -it debian-ansible